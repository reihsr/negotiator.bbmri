package de.samply.bbmri.mailing;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MailSendingTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getHost() {
    }

    @Test
    void setHost() {
    }

    @Test
    void getProtocol() {
    }

    @Test
    void setProtocol() {
    }

    @Test
    void getPort() {
    }

    @Test
    void setPort() {
    }

    @Test
    void getFromAddress() {
    }

    @Test
    void setFromAddress() {
    }

    @Test
    void getFromName() {
    }

    @Test
    void setFromName() {
    }

    @Test
    void getUser() {
    }

    @Test
    void setUser() {
    }

    @Test
    void getPassword() {
    }

    @Test
    void setPassword() {
    }

    @Test
    void getTemplateFolder() {
    }

    @Test
    void setTemplateFolder() {
    }
}